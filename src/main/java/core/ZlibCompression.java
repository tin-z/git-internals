package core;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

public class ZlibCompression {

	/*
	 * Decompress a zlib compressed file.
	 */
	public static String decompressFile(String complete_path)
	{
		try {
			File compres = new File(complete_path);
			InputStream in;
			
			in = new InflaterInputStream( new FileInputStream(compres) );
			
			byte [] bufferZ = new byte[4096];
			ByteArrayOutputStream outme = new ByteArrayOutputStream();
			
			int read;
			while((read = in.read(bufferZ, 0, bufferZ.length)) != -1) {
				outme.write(bufferZ, 0, read);
			} 
			return outme.toString("UTF-8");
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
}
