package core;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.function.IntPredicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GitBlobObject {

	private GitRepository repoMe;
	private String blobMe;
	private String blobMeFile;
	
	public GitBlobObject(String repoPath, String string) {
		repoMe = new GitRepository(repoPath);
		blobMe = string;
		blobMeFile = repoMe.getRepo() + "/" + "objects/" + blobMe.substring(0, 2) + "/" + blobMe.substring(2);
	}

	public String getContent() {
		String ret = "";
		
		ret = ZlibCompression.decompressFile(blobMeFile);
		Pattern pattern_head = Pattern.compile("blob [0-9]+.*\n((.*\n)+)");
		Matcher matcher_head = pattern_head.matcher(ret);
		if(matcher_head.find())
			ret = matcher_head.group(1);
		
		return ret;
	}

	public String getType() {		
		String ret = "";
		ret = ZlibCompression.decompressFile(blobMeFile);
		Pattern pattern_head = Pattern.compile("(blob) .*");
		Matcher matcher_head = pattern_head.matcher(ret);
		if(matcher_head.find())
			ret = matcher_head.group(1);
		
		return ret;
		
	}

}
