package core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.function.IntPredicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GitRepository {

	String repoMe;

	public GitRepository(String repoPath) {
		repoMe = repoPath;
	}

	public String getHeadRef() {
		BufferedReader br = null;
		FileReader fr = null;
		String ret = readOneLine("HEAD", br, fr);
		Pattern pattern_head = Pattern.compile("^ref: (.*)");
		Matcher matcher_head = pattern_head.matcher(ret);
		if(matcher_head.find())
			ret = matcher_head.group(1);
		return ret;
	}

	public String getRefHash(String string) {
		BufferedReader br = null;
		FileReader fr = null;
		return readOneLine(string, br, fr);
	}

	public String readOneLine(String string, BufferedReader br, FileReader fr) {
		String ret = "";
		try {
			fr = new FileReader(this.repoMe + "/" +string);
			br = new BufferedReader(fr);
			ret = br.readLine();

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
				if (fr != null)
					fr.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return ret;
	}
	
	public String getRepo()
	{
		return repoMe;
	}
}