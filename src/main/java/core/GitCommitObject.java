package core;

import java.util.function.IntPredicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GitCommitObject {

	private GitRepository repoMe;
	private String commitMe;
	private String commitMeFile;
		
	public GitCommitObject(String repoPath, String masterCommitHash) {
		repoMe = new GitRepository(repoPath);
		commitMe= masterCommitHash;
		commitMeFile = repoMe.getRepo() + "/" + "objects/" + commitMe.substring(0, 2) + "/" + commitMe.substring(2);
	}

	public String getHash() {
		return commitMe;
		
	}

	public String getTreeHash() {
		String pattern = "tree (.*)";
		return getSome(pattern);
	}

	private String getSome(String pattern) {
		for(String str : ZlibCompression.decompressFile(commitMeFile).split("\n")) {
			Pattern pattern_head = Pattern.compile(pattern);
			Matcher matcher_head = pattern_head.matcher(str);
			if(matcher_head.find())
				return matcher_head.group(1);
		}
		return null;
	}

	public String getParentHash() {
		String pattern = "parent (.*)";
		return getSome(pattern);
	}

	public String getAuthor() {
		String pattern = "committer (.*>).*";
		return getSome(pattern);
	}

}
